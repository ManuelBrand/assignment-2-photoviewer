/**
 * Created by manue_000 on 2/9/2015.
 */


(function () {
    "use strict";


    window.assignment = {
        start: function() {
            var tileCount, body, content, canvas;
            tileCount = 9;

            // Name body, Create content window
            body = document.getElementsByTagName("body")[0];
            content = document.createElement("section");
            content.className = "content";
            body.appendChild(content);

            // Create 9 tiles for placement in content
            for (var i = 0; i < tileCount; i++) {
                var canvases = new Array();
                    canvas = document.createElement("canvas");
                    canvas.id = "id" + i;
                canvases.push(canvas);
                console.log(canvas);
                content.appendChild(canvas);

            };

            // Fill content tiles


            /*
            var bdy, newDiv, editDiv;
            bdy = document.getElementsByTagName("body")[0];
            console.log(bdy);

            // CREATE
            newDiv = document.createElement("div");
            newDiv.id = "id1";
            newDiv.textContent = "Hello World!";
            console.log(newDiv);

            // ADD
            bdy.appendChild(newDiv);

            // UPDATE
            editDiv = document.getElementById("id1");
            editDiv.textContent = "CLICK FOR BLUE";
            editDiv.style.color = "red";
            editDiv.style.fontsize = "50";

            // DELETE

            // EVENTLISTENER
            newDiv.addEventListener("click", function() {
                newDiv.style.color = "blue";

            });
            */
        }
    };

}());


window.addEventListener("load", assignment.start);