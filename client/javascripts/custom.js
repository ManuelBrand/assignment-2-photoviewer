/**
 * CRIA-WT Photoviewer 30/3/2015
 * Manuel Brand 474283
 */

var photoviewer = window.photoviewer;
var YoutubeVideo = window.YoutubeVideo;

(function () {

    "use strict";

    window.photoviewer = {


        settings: {
            proxy: "http://server7.tezzt.nl:1332/api/proxy",
            rows: 5,
            columns: 5
        },

        tileData: {
            width: 0,
            height: 0
        },

        tiles: [],

        r: function (min, max) {
            return (Math.floor(Math.random() * (max - min) + min)
            );
        },

        createVideoNode: function () {
            var bodyNode, videoNode, cb, youtubeId;
            bodyNode = document.querySelector("body");
            videoNode = document.createElement("video");
            videoNode.id = "myVid";
            videoNode.autoplay = "autoplay";
            videoNode.volume = 0;
            youtubeId = '0MTBTIkZfr0';

            videoNode.addEventListener("loadeddata", function () {
                bodyNode.appendChild(videoNode);
                photoviewer.createTiles();

                // Set video refreshrate to 20 MS
                window.setInterval(photoviewer.fillTiles, 20);

            });

            cb = function (video) {
                var webm;
                webm = video.getSource("video/webm", "medium");
                // var mp4 = video.getSource("video/mp4", "medium");
                videoNode.src = webm.url;
            };
            YoutubeVideo(youtubeId, photoviewer.settings.proxy, cb);

        },

        createTiles: function () {
            var rowCounter, columnCounter, tileName, tileNumber, tileWidth, tileHeight, tileBox, tileContainer, bodyNode, tile;

            tileWidth = Math.floor(document.querySelector("#myVid").videoWidth / photoviewer.settings.columns) - 2;
            tileHeight = Math.floor(document.querySelector("#myVid").videoHeight / photoviewer.settings.rows) - 2;
            tileNumber = 0;

            tileContainer = document.createElement("div");
            tileContainer.className = "tileContainer";

            for (rowCounter = 0; rowCounter < photoviewer.settings.rows; rowCounter += 1) {
                for (columnCounter = 0; columnCounter < photoviewer.settings.columns; columnCounter += 1) {

                    tileName = "tile" + tileNumber;
                    tileBox = document.createElement("canvas");
                    tileBox.id = tileName;
                    tileBox.width = tileWidth;
                    tileBox.height = tileHeight;
                    tileBox.className = "tile";
                    tileBox.addEventListener("click", photoviewer.clickHandler);
                    tileContainer.appendChild(tileBox);

                    tile = {
                        name: "tile " + rowCounter + columnCounter,
                        column: columnCounter,
                        row: rowCounter,
                        sequence: tileNumber,
                        display: true
                    };

                    photoviewer.tiles.push(tile);
                    tileNumber += 1;
                }
            }
            photoviewer.shuffleArray(photoviewer.tiles);

            // Set last item in array as voidTile
            photoviewer.tiles[photoviewer.tiles.length - 1].display = false;
            bodyNode = document.querySelector("body");
            bodyNode.appendChild(tileContainer);
        },

        //+ Jonas Raoni Soares Silva
        //@ http://jsfromhell.com/array/shuffle [v1.0]

        // NOTE: this code is NOT mine, JSLint has some commentary but i will not modify this piece of code.
        // If LSint stopt here with checking, try commenting the entire function and running lint again.
        shuffleArray: function shuffle(o) {
            for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
            return o;
        },

        fillTiles: function () {
            var tile, context, tileCounter, videoId, img, i, j;
            tileCounter = 0;
            videoId = document.getElementById("myVid");
            img = new Image();
            img.src = "./images/1x1.png";

            for (i = 0; i < photoviewer.settings.rows; i += 1) {
                for (j = 0; j < photoviewer.settings.columns; j += 1) {
                    tile = document.getElementsByTagName("canvas")[tileCounter];
                    context = tile.getContext("2d");

                    // Check if void tile
                    if (photoviewer.tiles[tileCounter].display !== false) {
                        context.drawImage(videoId, photoviewer.tiles[tileCounter].column * tile.offsetWidth, photoviewer.tiles[tileCounter].row * tile.offsetHeight, tile.offsetWidth, tile.offsetHeight,
                            0, 0, tile.offsetWidth, tile.offsetHeight);
                    } else {
                        //context.rect(0, 0, tile.offsetWidth, tile.offsetHeight);
                        context.drawImage(img, 0, 0, tile.offsetWidth, tile.offsetHeight);
                    }
                    tileCounter += 1;
                }
            }
            photoviewer.checkWinCondition();

        },

        init: function () {
            var h1Node, bodyNode;
            bodyNode = document.querySelector("body");
            h1Node = document.createElement("h1");
            h1Node.textContent = "Video sliding puzzle";
            bodyNode.appendChild(h1Node);
            document.addEventListener("keyup", photoviewer.keyHandler);
        },

        run: function () {
            photoviewer.init();
            photoviewer.createVideoNode();
        },

        clickHandler: function (e) {
            var position, temp;
            position = String(e.target.id);
            position = parseInt(position.slice(4, 6), 10);

            // move void tile to -
            // right
            if (position > 0 && position % photoviewer.settings.rows !== 0 && photoviewer.tiles[position - 1].display === false) {
                temp = photoviewer.tiles[position - 1];
                photoviewer.tiles[position - 1] = photoviewer.tiles[position];
                photoviewer.tiles[position] = temp;
                return;
            }
            // left
            if (position < photoviewer.tiles.length - 1 &&
                    (position + 1) % photoviewer.settings.rows !== 0 &&
                    photoviewer.tiles[position + 1].display === false) {
                temp = photoviewer.tiles[position + 1];
                photoviewer.tiles[position + 1] = photoviewer.tiles[position];
                photoviewer.tiles[position] = temp;
                return;
            }

            // down
            if (position >= photoviewer.settings.rows &&  photoviewer.tiles[position - photoviewer.settings.rows].display === false) {
                temp = photoviewer.tiles[position - photoviewer.settings.columns];
                photoviewer.tiles[position - photoviewer.settings.columns] = photoviewer.tiles[position];
                photoviewer.tiles[position] = temp;
                return;
            }
            // up
            if (position + photoviewer.settings.rows < photoviewer.tiles.length &&
                    photoviewer.tiles[position + photoviewer.settings.rows].display === false) {
                temp = photoviewer.tiles[position + photoviewer.settings.columns];
                photoviewer.tiles[position + photoviewer.settings.columns] = photoviewer.tiles[position];
                photoviewer.tiles[position] = temp;
                return;
            }

        },

        keyHandler: function (e) {
            var temp, i;

            // UP 38
            if (e.keyCode === 38) {
                for (i = 0; i < photoviewer.tiles.length; i += 1) {
                    if (photoviewer.tiles[i].display === false && i >= photoviewer.settings.rows) {
                        temp = photoviewer.tiles[i - photoviewer.settings.columns];
                        photoviewer.tiles[i - photoviewer.settings.columns] = photoviewer.tiles[i];
                        photoviewer.tiles[i] = temp;
                        return;
                    }
                }
            }
            // DOWN 40
            if (e.keyCode === 40) {
                for (i = 0; i < photoviewer.tiles.length; i += 1) {
                    if (photoviewer.tiles[i].display === false && i < photoviewer.tiles.length - photoviewer.settings.rows) {
                        temp = photoviewer.tiles[i + photoviewer.settings.columns];
                        photoviewer.tiles[i + photoviewer.settings.columns] = photoviewer.tiles[i];
                        photoviewer.tiles[i] = temp;
                        return;
                    }
                }
            }
            // RIGHT 39
            if (e.keyCode === 39) {
                for (i = 0; i < photoviewer.tiles.length; i += 1) {
                    if (photoviewer.tiles[i].display === false && (i + 1) % photoviewer.settings.rows > 0) {
                        temp = photoviewer.tiles[i + 1];
                        photoviewer.tiles[i + 1] = photoviewer.tiles[i];
                        photoviewer.tiles[i] = temp;
                        return;
                    }
                }
            }
            // LEFT 37
            if (e.keyCode === 37) {
                for (i = 0; i < photoviewer.tiles.length; i += 1) {
                    if (photoviewer.tiles[i].display === false && i % photoviewer.settings.rows > 0) {
                        temp = photoviewer.tiles[i - 1];
                        photoviewer.tiles[i - 1] = photoviewer.tiles[i];
                        photoviewer.tiles[i] = temp;
                        return;
                    }
                }
            }
        },

        checkWinCondition: function () {
            var seqCounter, i;
            seqCounter = 0;
            for (i = 0; i < photoviewer.tiles.length; i += 1) {
                if (photoviewer.tiles[i].sequence === i) {
                    seqCounter += 1;
                } else {
                    seqCounter = 0;
                }
            }

            if (seqCounter === photoviewer.tiles.length) {
                alert("you win congrats");
                location.reload();
            }
        }

    };


}());

window.addEventListener("load", photoviewer.run);